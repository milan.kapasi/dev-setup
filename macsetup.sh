echo 'export PATH="/usr/local/bin:$PATH"' >> ~/.bash_profile
xcode-select --install
# Install Brew
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
brew tap caskroom/cask
brew cask install \
    qlcolorcode \
    qlstephen \
    qlmarkdown \
    quicklook-json \
    qlprettypatch \
    quicklook-csv \
    betterzip \
    webpquicklook \
    suspicious-package
brew cask install google-chrome
brew cask install visual-studio-code
brew cask install iterm2
cat << EOF >> ~/.bash_profile
# Add Visual Studio Code (code)
export PATH="\$PATH:/Applications/Visual Studio Code.app/Contents/Resources/app/bin"
EOF
brew tap caskroom/fonts && brew cask install font-source-code-pro
brew install zsh