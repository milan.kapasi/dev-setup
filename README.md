# Dev Setup
# Mac Setup
Mac’s foundation is very strong with unix interface and widespread support for all development tools. However there are still many other packages that will make your dev environment on mac very efficient. we have included few popular guides below.

- Few of the commands are specified directly in the respective shell scripts, to be run in the terminal window of Mac


- [https://sourabhbajaj.com/mac-setup/](https://sourabhbajaj.com/mac-setup/)
- [https://github.com/donnemartin/dev-setup](https://github.com/donnemartin/dev-setup)
- [https://github.com/nicolashery/mac-dev-setup](https://github.com/nicolashery/mac-dev-setup)